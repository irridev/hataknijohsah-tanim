import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      dark: {
        primary: colors.green.darken3,
        secondary: colors.teal.darken1,
        // accent: '#82B1FF',
        // error: '#FF5252',
        // info: '#2196F3',
        // success: '#4CAF50',
        // warning: '#FFC107',
      },
      light: {
        primary: colors.green.darken3,
        secondary: colors.teal.darken1,
      },
    },
  },
  icons: {
    iconfont: 'mdi',
  },
});
