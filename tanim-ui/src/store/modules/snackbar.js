import _ from 'lodash';

const createSnackbarTrigger = defaultOptions => ({ commit }, data) => {
  let { message } = data;
  const { options } = data;

  if (message === undefined) {
    message = data;
  }

  commit('SET_SNACK', {
    message,
    options: {
      ...options,
      ...defaultOptions,
      bottom: true,
      right: true,
    },
  });
};

export default {
  namespaced: true,

  state: {
    message: '',
    options: {},
  },

  mutations: {
    SET_SNACK: (state, { message, options }) => {
      state.message = message;
      _.merge(state.options, options);
    },

    EMPTY_MESSAGE: (state) => {
      state.message = '';
    },
  },

  actions: {
    showSuccess: createSnackbarTrigger({
      color: 'success',
    }),
    showError: createSnackbarTrigger({
      color: 'error',
    }),
    showInfo: createSnackbarTrigger({
      color: 'info',
    }),
    showWarning: createSnackbarTrigger({
      color: 'warning',
    }),
  },
};
