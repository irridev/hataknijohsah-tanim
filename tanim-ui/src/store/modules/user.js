import api from '@/api';


export default {
  namespaced: true,

  state: {
    loggedInUser: {},
  },

  mutations: {
    SET_LOGGED_IN_USER: (state, data) => {
      state.loggedInUser = data;
    },
  },

  actions: {
    getLoggedInUser: async ({ commit }, userId) => {
      const { data } = await api.user.getUser(userId);

      commit('SET_LOGGED_IN_USER', data);
    },

    getPreference: async (context, userId) => {
      const { data } = await api.user.getPreference(userId);

      return data;
    },

    updatePreference: async (context, { userId, ...body }) => {
      await api.user.updatePreference(userId, body);
    },
  },
};
