import api from '@/api';

export default {
  namespaced: true,

  actions: {
    async fetch() {
      const { data } = await api.proposals.get();

      return data;
    },
  },
};
