import http from '@/services/http';

const resource = 'proposals';

export default {
  get: () => http.get(resource),
};
