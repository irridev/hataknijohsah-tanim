import http from '@/services/http';


export default {
  getUser: userId => http.get(`users/${userId}`),

  getPreference: userId => http.get(`users/${userId}/preference`),

  updatePreference: (userId, data) => http.post(`users/${userId}/preference`, data),
};
