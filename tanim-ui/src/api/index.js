import proposals from './proposals';
import user from './user';

export default {
  proposals,
  user,
};
