from django.contrib import admin

from tanim import models

class InvestorAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'name',
                    'location',
                    'description')
    list_per_page = 25

admin.site.register(models.Investor, InvestorAdmin)


class ProposalAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'crop',
                    'duration',
                    'budget')
    list_per_page = 25

admin.site.register(models.Proposal, ProposalAdmin)


class FarmerAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'name',
                    'location',
                    'description')
    list_per_page = 25

admin.site.register(models.Farmer, FarmerAdmin)
