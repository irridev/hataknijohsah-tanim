from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        print('CHECK IF USER EXISTS')
        if not User.objects.filter(username='adminhr').exists():
            print('CREATING USER')
            User.objects.create_superuser(username='adminhr',
                                          password='pass1234',
                                          email='helpdesk@tuitt.com')
