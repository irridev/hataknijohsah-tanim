from django.conf.urls import url
from django.urls import path

from tanim import views


urlpatterns = [
    url(r'^$', views.SwaggerSchemaView.as_view()),
    url(r'^investors$', views.InvestorList.as_view()),
    url(r'^investors/(?P<investor_id>[0-9a-f-]+)$', views.InvestorDetail.as_view()),
    url(r'^proposals$', views.ProposalList.as_view()),
    url(r'^proposals/(?P<proposal_id>[0-9a-f-]+)$', views.ProposalDetail.as_view()),
    url(r'^farmers$', views.FarmerList.as_view()),
    url(r'^farmers/(?P<farmer_id>[0-9a-f-]+)$', views.FarmerDetail.as_view())
]