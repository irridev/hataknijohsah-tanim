from django.db import models
from django.utils import timezone


class Investor(models.Model):
    name = models.CharField(max_length=64, blank=False, null=False)
    location = models.CharField(max_length=256, blank=True, null=True)
    description = models.CharField(max_length=512, blank=True, null=True)


class Proposal(models.Model):
    crop = models.CharField(max_length=32, blank=False, null=False)
    duration = models.CharField(max_length=64, blank=False, null=False)
    budget = models.CharField(max_length=64, blank=True, null=True)
    description = models.CharField(max_length=512, blank=True, null=True)
    location = models.CharField(max_length=256, blank=True, null=True)
    investigator = models.ForeignKey(Investor, blank=False, null=False, on_delete=models.CASCADE)


class Farmer(models.Model):
    name = models.CharField(max_length=64, blank=False, null=False)
    location = models.CharField(max_length=256, blank=True, null=True)
    description = models.CharField(max_length=512, blank=True, null=True)


class FarmerHistory(models.Model):
    finished_at = models.DateTimeField(blank=True, null=True)
    remarks = models.CharField(max_length=512, blank=True, null=True)

    proposal = models.ForeignKey(Proposal, blank=False, null=False, on_delete=models.CASCADE)
    farmer = models.ForeignKey(Farmer, blank=False, null=False, on_delete=models.CASCADE)
