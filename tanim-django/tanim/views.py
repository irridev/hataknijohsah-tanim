import traceback

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers

# Create your views here.

from tanim.models import Investor, Proposal, Farmer


class InvestorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Investor
        fields = '__all__'


class ProposalSerializer(serializers.ModelSerializer):

    class Meta:
        model = Proposal
        fields = '__all__'


class FarmerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Farmer
        fields = '__all__'


class InvestorList(APIView):

    def get(self, request):
        try:
            instances = Investor.objects.all()
            serializer = InvestorSerializer(instances, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        try:
            serializer = InvestorSerializer(data=request.data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class InvestorDetail(APIView):

    def get(self, request, investor_id):
        try:
            instance = Investor.objects.get(pk=investor_id)
            serializer = InvestorSerializer(instance)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, investor_id):
        try:
            instance = Investor.objects.get(pk=investor_id)
            serializer = InvestorSerializer(instance, data=request.data, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProposalList(APIView):

    def get(self, request):
        try:
            instances = ProposalGuarantor.objects.all()
            serializer = ProposalSerializer(instances, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        try:
            serializer = ProposalSerializer(data=request.data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProposalDetail(APIView):

    def get(self, request, proposal_id):
        try:
            instance = Proposal.objects.get(pk=proposal_id)
            serializer = ProposalSerializer(instance)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, proposal_id):
        try:
            instance = Proposal.objects.get(pk=proposal_id)
            serializer = ProposalSerializer(instance, data=request.data, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class FarmerList(APIView):

    def get(self, request):
        try:
            instances = Farmer.objects.all()
            serializer = FarmerSerializer(instances, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        try:
            serializer = FarmerSerializer(data=request.data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class FarmerDetail(APIView):

    def get(self, request, farmer_id):
        try:
            instance = Farmer.objects.get(pk=farmer_id)
            serializer = FarmerSerializer(instance)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, farmer_id):
        try:
            instance = Farmer.objects.get(pk=farmer_id)
            serializer = FarmerSerializer(instance, data=request.data, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as exc:
            print(str(exc))
            print(traceback.format_exc())

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


from rest_framework.permissions import AllowAny
from rest_framework.schemas import SchemaGenerator
from rest_framework_swagger import renderers


class SwaggerSchemaView(APIView):
    permission_classes = [AllowAny]
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        generator = SchemaGenerator()
        schema = generator.get_schema(request=request)

        return Response(schema)